//
//  ViewController.m
//  TheySeeMeScrolling
//
//  Created by James Cash on 23-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) NSArray<UIImage*>* phoneImages;
@property (strong, nonatomic) UIImageView * airpodsView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Load phone images for scrolling
    self.phoneImages = @[[UIImage imageNamed:@"black"],
                         [UIImage imageNamed:@"gold"],
                         [UIImage imageNamed:@"rosegold"],
                         [UIImage imageNamed:@"silver"]];
    // Load airpod image for zooming
    self.airpodsView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"airpods"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    // Choose one or the other to comment out
//    [self setUpScrollViewForPhones];
    [self setUpScrollViewForAirpods];
}

- (void)setUpScrollViewForPhones
{
    CGRect viewRect = self.view.bounds;
    CGFloat imageY = 0;
    // Make an uiimageview for each phone image
    for (UIImage *phoneImage in self.phoneImages) {
        // Create image view with image
        UIImageView *imgView = [[UIImageView alloc] initWithImage:phoneImage];
        // Set it to scale reasonably so images don't get distorted
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        // Frame = size of the view, moved 0 on x-axis & imageY on y-axis
        CGRect imgFrame = CGRectOffset(viewRect, 0, imageY);
        imgView.frame = imgFrame;
        // Increase imageY by the height of one view, so next view is below this one
        imageY += CGRectGetHeight(viewRect);
        // Add each as a subview of the scrollview
        [self.scrollView addSubview:imgView];
    }
    // Make scrollview move in discrete pages
    self.scrollView.pagingEnabled = YES;
    // Set the content size of the scrollview
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(viewRect),
                                             CGRectGetHeight(viewRect) * self.phoneImages.count);
}

- (void)setUpScrollViewForAirpods
{
    // Add the airpods image to the scrollView
    [self.scrollView addSubview:self.airpodsView];
    // Set the content size to just be the size of the image
    self.scrollView.contentSize = self.airpodsView.frame.size;
    // We need to use a delegate method for zooming to work, so we set ourselves as the delegate
    self.scrollView.delegate = self;
    // By default, min & max zoom scales are both 1, so we need to set some values so we can actually zoom
    self.scrollView.maximumZoomScale = 3.0;
    self.scrollView.minimumZoomScale = 0.25;
}

// Need to implement this method for zooming to work - the scrollview needs to know which view to zoom in on
// Note that this ties into the previously-mentioned notion that if you have multiple views in a scrollview that you want to scroll in, it makes most sense to nest those subviews
// [ScrollView]
//   -> [Container UIView] <--- return this container in method below
//      -> image 1
//      -> image 2
//      -> etc
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.airpodsView;
}

@end
